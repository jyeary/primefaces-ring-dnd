/*
 * Copyright 2013 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.example.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.DragDropEvent;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
@ViewScoped
public class IndexBean implements Serializable {

    private List<Arrow> arrows;
    private List<Arrow> firedArrows;
    private Arrow selectedArrow;

    public IndexBean() {
        arrows = new ArrayList<Arrow>(10);
        firedArrows = new ArrayList<Arrow>(10);

        for (long n = 0; n < 10L; n++) {
            arrows.add(new Arrow(n, "Arrow " + n, "I am an Arrow. There are many arrows like me, but I am number #" + n));
        }
    }

    public Arrow getSelectedArrow() {
        return selectedArrow;
    }

    public void setSelectedArrow(Arrow selectedArrow) {
        this.selectedArrow = selectedArrow;
    }

    public List<Arrow> getArrows() {
        return arrows;
    }

    public void setFiredArrows(List<Arrow> firedArrows) {
        this.firedArrows = firedArrows;
    }

    public List<Arrow> getFiredArrows() {
        return firedArrows;
    }

    public void onArrowDrop(DragDropEvent ddEvent) {
        Arrow arrow = ((Arrow) ddEvent.getData());
        firedArrows.add(arrow);
        arrows.remove(arrow);
    }
}
